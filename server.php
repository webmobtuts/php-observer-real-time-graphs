<?php
require_once 'vendor/autoload.php';
use Workerman\Worker;
use Workerman\WebServer;
use Workerman\Autoloader;
use PHPSocketIO\SocketIO;

$subject = new Subject();

$columnObserver = new ColumnGraphObserver();
$lineObserver = new LineGraphObserver();
$pieObserver = new PieGraphObserver();

$subject->attach($lineObserver);
$subject->attach($columnObserver);
$subject->attach($pieObserver);

$io = new SocketIO(2020);

$io->on('connection', function($socket) use ($subject) {
    $subject->notify();

    $socket->on('refresh_graphs', function () use($subject) {
        $subject->notify();
    });
});

Worker::runAll();