window.onload = function () {

    var charts = [
        {type: 'line', selector: 'lineContainer', instance: '', dataPoints: []},
        {type: 'column', selector: 'columnContainer', instance: '', dataPoints: []},
        {type: 'pie', selector: 'pieContainer', instance: '', dataPoints: []}
    ];

    var websocketport = '2020';
    var updateInterval = 2000;

    for(var i=0; i<charts.length; i++) {
        charts[i].instance = new CanvasJS.Chart(charts[i].selector, {
            title: {
                text: charts[i].type + " chart"
            },
            axisY: {
                includeZero: false
            },
            data: [{
                type: charts[i].type,
                dataPoints: charts[i].dataPoints
            }]
        });
    }

    for (var i=0; i<charts.length; i++) {
       charts[i].instance.render();
    }

    let socket = io('http://'+document.domain+':' + websocketport);

    socket.on('lineGraph', function(data) {
        clearArray(charts[0].dataPoints);

        for(var i=0; i<data.dataPoints.length; i++) {
                charts[0].dataPoints.push(data.dataPoints[i]);
        }
        charts[0].instance.render();
    });

    socket.on('columnGraph', function(data) {
        clearArray(charts[1].dataPoints);

        for(var i=0; i<data.dataPoints.length; i++) {
            charts[1].dataPoints.push(data.dataPoints[i]);
        }
        charts[1].instance.render();
    });

    socket.on('pieGraph', function(data) {
        clearArray(charts[2].dataPoints);

        for(var i=0; i<data.dataPoints.length; i++) {
            charts[2].dataPoints.push(data.dataPoints[i]);
        }
        charts[2].instance.render();
    });

    setInterval(function () {
        socket.emit('refresh_graphs', 1);
    }, updateInterval);

    clearArray = function (arr) {
        while(arr.length > 0) {
            arr.pop();
        }
    }
}