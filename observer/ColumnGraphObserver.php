<?php

class ColumnGraphObserver implements SplObserver
{
    public function update(SplSubject $subject)
    {
        global $io;

        $dataPoints = [];
        $xVal = 0;
        $yVal = 100;

        for($i=0; $i < 9; $i++) {
            $yVal = $yVal + floor(rand(1, 1000));

            $dataPoints[] = ['x' => $xVal, 'y' => $yVal, 'label' => "[$xVal, $yVal]"];

            $xVal++;
            $yVal = 0;
        }

        if(count($dataPoints) > 9) {
            array_shift($dataPoints);
        }

        // emit the data points
        $io->emit('columnGraph', array('dataPoints' => $dataPoints));
    }
}