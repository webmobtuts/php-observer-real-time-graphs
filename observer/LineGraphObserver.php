<?php

class LineGraphObserver implements SplObserver
{
    public function update(SplSubject $subject)
    {
        global $io;

        $dataPoints = [];
        $xVal = 0;
        $yVal = 100;

        for($i=0; $i<12; $i++) {
            $yVal = $yVal + floor(rand(1, 1000));

            $dataPoints[] = ["x" => $xVal, "y" => $yVal];

            $xVal++;
            $yVal = 0;
        }

        if(count($dataPoints) > 12) {
            array_shift($dataPoints);
        }

        // emit the data points to socket server
        $io->emit('lineGraph', array('dataPoints' => $dataPoints));
    }
}